import React from 'react';
import axios from 'axios';
import Navbar from './Components/Navbar';
import Dashboard from './Components/Dashboard';
import Brands from './Components/Brands';
import Category from './Components/Category';
import Products from './Components/Products';
import Footer from './Components/Footer';
import Loader from './Components/Loader';
import './reset.css';
import './App.css';

export default class App extends React.Component {

  constructor() {
    super();
    this.state = {
      products: [],
      productsRecieved: false,
      productsNotRecieved: false,
      productsError: "",
      categories: {},
      category: [],
    }
  }

  async fetchProducts() {
    try {
      const productsStatus = this.state.productsRecieved;
      const productsResult = await axios.get('https://fakestoreapi.com/products')
      const productsData = productsResult.data;
      console.log(productsData);
      const addProducts = await axios.post('https://fakestoreapi.com/products', {
        title: 'test product',
        price: 13.5,
        description: 'lorem ipsum set',
        image: 'https://i.pravatar.cc',
        category: 'electronic'
      })
      const newProductsData = addProducts.data;
      productsData.push(newProductsData);
      this.setCategory(productsData);
      this.setState({
        products: productsData,
        productsRecieved: !productsStatus,
      })
    } catch (err) {
      const productsStatus = this.state.productsRecieved;
      const productsNotRecievedStatus = this.state.productsNotRecieved;
      this.setState({
        productsError: "Unfortunately! Products not found!",
        productsRecieved: !productsStatus,
        productsNotRecieved: !productsNotRecievedStatus,
      })
    }
  }

  async updateProduct(newProduct) {
    console.log(newProduct);
    const updateData = await axios.patch(`https://fakestoreapi.com/products/${newProduct.id}`,
      {
        title: newProduct.title,
        price: newProduct.price,
        description: newProduct.description,
        image: 'https://i.pravatar.cc',
        category: "men's clothing",
      }
    )
    const updatedData = updateData.data;
    console.log(updatedData);
    let updatedProduct = this.state.products;
    updatedProduct[newProduct.id - 1] = updatedData;
    const newUpdatedProduct = updatedProduct.map((el) => {
      if(el.id === updatedProduct.id) {
        return updatedProduct;
      } else {
        return el;
      }
    })

    this.setCategory(newUpdatedProduct);
  }

  setCategory(products) {
    console.log("Inside setCategory");
    const filterData = products.reduce((acc, product) => {
      if (acc[product.category] === undefined) {
        acc[product.category] = [product]
      } else {
        acc[product.category].push(product);
      }
      return acc;
    }, {})

    this.setState({
      products: products,
      categories: filterData,
      category: filterData["men's clothing"],
    })
  }

  async deleteProduct(id) {

    const deleteProduct = await axios.delete(`https://fakestoreapi.com/products/${id}`)
    const deletedProduct = deleteProduct.data;
    const previousProducts = this.state.products;
    const newDeletedData = previousProducts.filter((product) => {
      return product.id !== deletedProduct.id;
    })

    this.setCategory(newDeletedData);
  }

  changeCategory(productCategory) {
    this.setState({
      category: this.state.categories[productCategory],
    })
  }

  componentDidMount() {
    this.fetchProducts();
  }

  render() {
    return (
      <div className="App">
        {
          this.state.productsRecieved ? (
            this.state.productsNotRecieved ?
              <h1 className='h1 text-center text-dark m-5'>{this.state.productsError}</h1>
              :
              this.state.products.length === 0 ?
                <h1 className='h1 text-center text-dark m-5'>Sorry! No products available.</h1>
                :
                <div>
                  <Navbar />
                  <Dashboard onClick={() => console.log("clicked")} />
                  <Brands />
                  <Category categories={this.state.category} changeCategory={(category) => this.changeCategory(category)} />
                  <Products products={this.state.products} updateProduct={(updatedProduct) => this.updateProduct(updatedProduct)} deleteProduct={(id) => this.deleteProduct(id)} />
                  <Footer />
                </div>
          )
            :
            <Loader />
        }
      </div>
    )
  }
}