import React from "react";
import './styles/category.css';

export default class Category extends React.Component {

    render() {
        return (
            <div className="bg-light py-3 text-center" id="category">
                <h1 className="h1 text-center">Best selling product</h1>
                <ul className="d-flex justify-content-center">
                    <li className="p-2" onClick={() => this.props.changeCategory("men's clothing")}>Men's Wear</li>
                    <li className="p-2" onClick={() => this.props.changeCategory("women's clothing")}>Women's Wear</li>
                    <li className="p-2" onClick={() => this.props.changeCategory("jewelery")}>Jewelery</li>
                    <li className="p-2" onClick={() => this.props.changeCategory("electronics")}>Electronics</li>
                </ul>
                {(this.props.categories.length) ?
                    <div className="p-3 row">
                        {this.props.categories.slice(0, 4).map((product) => {
                            return (
                                <div key={product.id} className="my-2 col-sm-3">
                                    <div className="card h-100 align-items-center">
                                        <img src={product.image} alt="Product" className="card-img-top w-50 mt-4" style={{ "height": "25vh" }} />
                                        <div className="bg-white card-body d-flex flex-column justify-content-end align-items-center">
                                            <h6 className="h6 card-title">{product.title}</h6>
                                            <p className="p-2 card-text text-primary">&#36;{product.price}</p>
                                        </div>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                    :
                    <h1>Products not found</h1>
                }
            </div>
        )
    }
}