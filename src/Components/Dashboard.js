import React from "react";
import './styles/dashboard.css';

export default class Dashboard extends React.Component {

    render() {
        return (
            <div className="dashboard text-light d-flex text-start ">
                <div className="w-sm-50 my-sm-4 ms-3">
                    <div className="d-flex flex-sm-column justify-content-center">
                        <p className="py-1 w-50">S P O R T S</p>
                        <h1 className="h1">Easy and reliable purchase</h1>
                    </div>
                    <p className="py-sm-3">We provide various kinds of products for you, ranging from men's clothing, women's clothing, jewelery, and electronics</p>
                    <button className="btn btn-outline-light" type="button">Order Now</button>
                </div>
            </div>
        )
    }
}