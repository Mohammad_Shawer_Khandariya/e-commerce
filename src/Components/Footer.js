import React from "react";
import './styles/footer.css';

export default class Footer extends React.Component {

    render() {
        return (
            <div className="bg-dark text-light p-3 text-center text-sm-start row" id="about">
                <ul className="col-sm-3">
                    <img src={require('./images/brandlogo.png')} alt="Brand Logo" className="footer-image my-2" width="100px" />
                    <p className="py-1">Alton is located in a startegic place, if you want to visit our shop directly, it is located at Ratlam, Madhya Pradesh, 457001.</p>
                    <div className="d-flex justify-content-center align-items-center">
                        <img src={require('./images/app-store.png')} alt="App Store" className="" width="100px" />
                        <img src={require('./images/play-store.png')} alt="Google Play" className="" width="120px" />
                    </div>
                </ul>
                <ul className="col-sm-3">
                    <li className="my-3">Quick Link</li>
                    <li className="my-3">Collection</li>
                    <li className="my-3">About</li>
                    <li className="my-3">Shop</li>
                </ul>
                <ul className="col-sm-3">
                    <li className="my-3">Support</li>
                    <li className="my-3">Term & Condition</li>
                    <li className="my-3">Help</li>
                    <li className="my-3">FAQ</li>
                </ul>
                <ul className="col-sm-3">
                    <li className="my-3">Contact</li>
                    <li className="my-3">alto@alto.com</li>
                    <li className="my-3">+91-987654321</li>
                    <ul className="social-handle my-3 d-flex justify-content-center" >
                        <li><img src={require('./images/facebook.png')} alt="Facebook" width="20px" className="me-1" /></li>
                        <li><img src={require('./images/instagram.png')} alt="Instagram" width="20px" className="mx-1" /></li>
                        <li><img src={require('./images/twitter.png')} alt="WhatsApp" width="20px" className="ms-1" /></li>
                    </ul>
                </ul>
            </div>
        )
    }
}