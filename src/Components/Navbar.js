import React from "react";
import './styles/navbar.css';

export default class Navbar extends React.Component {

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light text-center" id="home">
                <div className="container-fluid">
                    <a href="#home" className="navbar-brand">
                        <img src={require('./images/brandlogo.png')} alt="Brand Logo" width="50%" height="50%" />
                    </a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mx-auto">
                            <li className="nav-item"><a href="#category" className="nav-link">Collection</a></li>
                            <li className="nav-item"><a href="#about" className="nav-link">About</a></li>
                            <li className="nav-item"><a href="#products" className="nav-link">Shop</a></li>
                            <li className="nav-item"><a href="#about" className="nav-link">Download apps</a></li>
                        </ul>
                        <div>
                            <button className="mx-1 btn btn-outline-dark" type="button">Register</button>
                            <button className="mx-1 btn btn-outline-dark" type="button">Login</button>
                        </div>
                    </div>
                </div>
            </nav>
        )
    }
}