import React from "react";
import './styles/products.css';
import ReactModal from "react-modal";

export default class Products extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            productsSize: 12,
            showProduct: {},
            id: "",
            title: "",
            price: "",
            description: "",
            category: "",
        }
        this.customStyles = {
            content: {
                top: '50%',
                left: '50%',
                right: 'auto',
                bottom: 'auto',
                marginRight: '-50%',
                transform: 'translate(-50%, -50%)',
            },
        };
    }

    addMoreProducts() {
        const currentProductsSize = this.state.productsSize;
        this.setState({
            productsSize: currentProductsSize + 6,
        })
    }

    toggleModal = (product) => {
        const tempObj = this.state.showProduct
        tempObj[product.id] = !tempObj[product.id]
        console.log(product.title);
        this.setState({
            showProduct: { ...tempObj },
            id: product.id,
            title: product.title,
            price: product.price,
            description: product.description,
            category: product.category,
        })
    }

    setValue(e) {
        // console.log(e);
        const target = e.target;
        console.log(this.state);
        this.setState({
            [target.name]: target.value,
        })
    }

    checkInput(editedValue) {
        let flag = true

        if (editedValue.title === "") {
            flag = false;
            return flag;
        }

        if (isNaN(editedValue.price) || editedValue.price === "") {
            flag = false;
            return flag;
        }

        if (editedValue.description === "") {
            flag = false;
            return flag;
        }

        return flag;
    }

    checkProduct(e) {

        if (this.checkInput(this.state)) {
            console.log("Successful");
            const updatedProduct = {
                id: this.state.id,
                title: this.state.title,
                price: this.state.price,
                description: this.state.description,
            }
            this.props.updateProduct(updatedProduct);
            this.toggleModal(updatedProduct);
        } else {
            console.log("Unsuccessful");
        }
        e.preventDefault();
    }

    render() {

        const products = this.props.products;

        return (
            <div className="p-2" id="products">
                <div className="m-2 d-flex justify-content-between">
                    <h1 className="h2">Our Product</h1>
                    <button type="button" className="btn btn-outline-primary" onClick={() => this.addMoreProducts()}>View more...</button>
                </div>
                {(products.length) ?
                    <div className="p-3 row">
                        {
                            (products.slice(0, this.state.productsSize).map((product) => {
                                return (
                                    <div key={product.id} className="col-6 col-md-3 col-sm-4 col-lg-2 my-2">
                                        <div className="card h-100 align-items-center bg-light">
                                            <img src={product.image} alt="Product" className="card-img-top w-50 mt-4" style={{ "height": "100px" }} />
                                            <div className="card-body d-flex flex-column justify-content-between align-items-center">
                                                <h6 className="h6 card-title">{product.title}</h6>
                                                <h3 className="card-text text-primary p-1">&#36;{product.price}</h3>
                                            </div>
                                            {/* <button onClick={() => this.props.updateProduct(product.id)}>Edit</button> */}
                                            {/* <!-- Button trigger modal --> */}
                                            <button type="button" className="btn btn-primary" id={product.id}
                                                onClick={() => this.toggleModal(product)}>
                                                Edit
                                            </button>

                                            <button type="button" className="btn btn-danger" id={product.id} 
                                                onClick={() => this.props.deleteProduct(product.id)}
                                            >
                                                Delete
                                            </button>

                                            <ReactModal isOpen={this.state.showProduct[product.id]} style={this.customStyles} ariaHideApp={false} >
                                                <div className="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                                    <form className="w-100" onSubmit={(event) => this.checkProduct(event)} >
                                                        <div className="modal-content">
                                                            <div className="modal-header">
                                                                <h5 className="modal-title" id="exampleModalLabel">Edit product</h5>
                                                                <button type="button" className="btn-close" aria-label="Close" onClick={() => { this.toggleModal(product) }} ></button>
                                                            </div>
                                                            <div className="modal-body">
                                                                <div className="container-fluid">
                                                                    <div className="form-container my-3">
                                                                        <label htmlFor="title" className="form-label" >Title: <span className="text-danger"></span></label>
                                                                        <input type="text" className="form-control" name="title" id="title"
                                                                            defaultValue={this.state.title}
                                                                            onChange={(event) => this.setValue(event)}
                                                                            placeholder="Enter Full Name" />
                                                                    </div>
                                                                    <div className="form-container my-3">
                                                                        <label htmlFor="price">Price: <span className="text-danger"></span></label>
                                                                        <input type="numb
                                                                {/* </form> */}er" className="form-control" name="price" id="price"
                                                                            defaultValue={this.state.price}
                                                                            onChange={(event) => this.setValue(event)}
                                                                            placeholder="Enter price" />
                                                                    </div>
                                                                    <select className="form-select" aria-label="Default select example" name="category" >
                                                                        <option defaultValue="men's clothing">Men's clothing</option>
                                                                        <option defaultValue="women's clothing">Women's clothing</option>
                                                                        <option defaultValue="jewelery">Jewelery</option>
                                                                        <option defaultValue="electronics">Electronics</option>
                                                                    </select>
                                                                    <div className="form-container my-3">
                                                                        <label htmlFor="description" className="form-label" >Description: <span className="text-danger"></span></label>
                                                                        <textarea type="text" className="form-control" name="description" id="description"
                                                                            defaultValue={this.state.description}
                                                                            onChange={(event) => this.setValue(event)}
                                                                            placeholder="Enter description" >
                                                                        </textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="modal-footer">
                                                                <button type="button" className="btn btn-secondary" onClick={() => { this.toggleModal(product) }}>Close</button>
                                                                <input type="submit" className="btn btn-primary" value="Save changes" />
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </ReactModal>
                                        </div>
                                    </div>
                                )
                            }))
                        }
                    </div>
                    :
                    <h2>Products not found!</h2>
                }
            </div>
        )
    }
}