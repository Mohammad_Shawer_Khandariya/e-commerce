import React from "react";
import './styles/why.css';

export default class Why extends React.Component {

    render () {
        return (
            <div className="why">
                <h1>Why Alton?</h1>
                <p>You can entrust all purchases of men's clothing, women's clothing, jewelery, and electronics items to us, we guarantee that all the items we sell are original products from well-known and trusted brands.</p>
                <button>Read more</button>
                <ul>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
        )
    }
}